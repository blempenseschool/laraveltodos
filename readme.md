# Oefening Laravel - Todo App (https://codeshare.io/apps-todo)
- Database
  tabel: todos / model Todo
  inhoud:
    - title : text
    - priority : int bv. 1, 2, 3, 4, 5 ...
    - completed : boolean
---
- Laravel: **composer create-project laravel/laravel --prefer-dist todoApp**
- Model: Todo **php artisan make:model Todo --migration**
- TodoController => **php artisan make:controller TodoController --resource**
---
## Login en Registratie systeem: **php artisan make:auth**
  geeft alle views, routes en code voor login en registreren + layout html en bootstrap link
---
## Relatie: User --> many Todos  / Todo --> belongs to User
---
## Content
  - index pagina voor de niet-completed todo's
  - create formulier voor todo toe te voegen (eventueel op de index pagina geplaatst)
  - een update pagina
  - delete pagina
  + "completed" tasks tonen op een pagina
  + taken gesorteerd of gefiltert op priority
