@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">To complete!</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Add a new todo!</h3>
                                @include('todos.form.add')
                            </div>
                            <div class="col-md-6">
                                @if(count($todos))
                                    <h3>Your tasks <small>(filtered by "{{ $priorities[$priority] }}")</small></h3>

                                    {!! Form::open(['route'=>'todos.filter', 'method'=>'get']) !!}
                                        {!! Form::label('filter', 'Filter by priority') !!}
                                        {!! Form::select('filter', $priorities) !!}
                                        {!! Form::submit('Filter') !!}
                                    {!! Form::close() !!}

                                    <a href="{{ route('todos.index') }}" class="btn btn-default">Back to list</a>

                                    <hr>
                                    <ul class="priority-list">
                                        @foreach($todos as $todo)
                                            <li class="{{ $todo->present()->priorityClass }} {{ $todo->present()->completedClass }}">
                                                {{ $todo->title }}
                                                <div class="priority-actions">
                                                    @if ($todo->completed != 1)
                                                        <a href="{{ route('todos.complete', $todo->id) }}" class="btn btn-default btn-xs">complete</a>
                                                    @endif
                                                    <a href="{{ route('todos.edit', $todo->id) }}" class="btn btn-info btn-xs">edit</a>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                @else
                                    <h3>No tasks with this priority found.</h3>
                                    <a href="{{ route('todos.index') }}" class="btn btn-default">Back to list</a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection