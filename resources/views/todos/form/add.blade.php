{!! Form::open(['route'=>'todos.store', 'class'=>'add-form']) !!}

    @include('todos.form.form')

    <p>
        {!! Form::submit('Create your todo') !!}
    </p>

{!! Form::close() !!}