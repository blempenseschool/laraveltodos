{!! Form::model($todo, [
    'method'=>'put',
    'route'=>['todos.update', $todo->id],
    'class'=>'edit-form'])
!!}

    @include('todos.form.form')

    <p>
        {!! Form::submit('Edit your todo') !!}
    </p>

{!! Form::close() !!}