<p>
    {!! Form::text('title', null, ['placeholder'=>'What to do?']) !!}
</p>
<p>
    <label for="priority">Priority</label> <br>
    Low <input value="{{ (isset($todo)) ? $todo->priority : '' }}" style="display: inline-block; width: auto;" type="range" min="1" max="5" name="priority" id="priority"> High
</p>