<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $sort = $request->get('s') ?: 'priority';
        $order = $request->get('o') ?: 'desc';
        $o = ($order == "asc") ? 'desc' : 'asc';

        $todos = $request->user()->todos()->orderBy($sort, $order)->get();
        return view('todos.index', compact('todos', 'o'));
    }

    public function complete(Todo $todo) {
        $todo->completed = true;
        $this->authorize('update', $todo);
        $todo->save();
        return redirect(route('todos.index'));
    }


    public function store(Request $request)
    {
        $todo = new Todo;
        $todo->title = $request->title;
        $todo->priority = $request->priority;

        $request->user()->todos()->save($todo);

        return redirect(route('todos.index'));
    }

    public function edit($id)
    {
        $todo = Todo::findOrFail($id);
        $this->authorize('update', $todo);

        return view('todos.edit', compact('todo'));
    }


    public function update(Request $request, $id)
    {
        $todo = Todo::findOrFail($id);
        $this->authorize('update', $todo);

        $todo->title = $request->title;
        $todo->priority = $request->priority;
        $todo->save();
        return redirect(route('todos.index'));
    }


    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $this->authorize('update', $todo);

        $todo->delete();
        return redirect(route('todos.index'));
    }

    public function filter(Request $request) {
        $priority = $request->filter;
        $todos = $request->user()->todos()->where('priority', $priority)->get();
        return view('todos.filter', compact('todos', 'priority'));
    }
}
