<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class TodoPresenter extends Presenter
{

    public function priorityClass()
    {
        switch ($this->priority) {
            case 1:
                $class = "priority-lowest";
                break;
            case 2:
                $class = "priority-low";
                break;
            case 3:
                $class = "priority-normal";
                break;
            case 4:
                $class = "priority-high";
                break;
            case 5:
                $class = "priority-highest";
        }

        return $class;
    }

    public function completedClass()
    {
        return ($this->completed == 1) ? 'task-completed' : '';
    }

}