<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function() {
   return redirect(route('todos.index'));
});

Auth::routes();

Route::get('todos/priority', 'TodoController@filter')->name('todos.filter');
Route::get('todos/{todo}/complete', 'TodoController@complete')->name('todos.complete');
Route::resource('todos', 'TodoController', ['except' => [
   'show'
]]);

//Route::get('/home', 'HomeController@index');