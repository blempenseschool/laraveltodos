<?php

use App\Todo;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->truncate();
        DB::table('todos')->truncate();

        $user = new User();
        $user->name = 'blempens';
        $user->email = 'blempens@cvodeverdieping.be';
        $user->password = bcrypt('secret');
        $user->save();

        $todo = new Todo();
        $todo->title = 'finish homework';
        $user->todos()->save($todo);

        $todo = new Todo();
        $todo->title = 'do laundry';
        $user->todos()->save($todo);

        $todo = new Todo();
        $todo->title = 'do the dishes';
        $user->todos()->save($todo);

        $todo = new Todo();
        $todo->title = 'walk the dog';
        $todo->priority = 5;
        $user->todos()->save($todo);
    }
}
